module.exports = {
	title: "Scanline",
	url: "https://scanline.fanstuff.garden/",
	language: "fr",
	description: "Un site de retrogaming vivant",
	author: {
		name: "Kazhnuz",
		email: "kazhnuz@kobold.cafe",
		url: "https://kazhnuz.space/"
	}
}
