---
layout: layouts/base.njk
eleventyNavigation:
  key: Accueil
  order: 0
---

# {{ metadata.title }}

Bienvenue dans {{ metadata.title }}, un petit site parlant d'émulation, de retrogaming, et de rétroinformatique.

Le but de ce site est de rendre plus accessible le retrogaming, et d'y avoir une approche non pas juste "nostalgique", mais réfléchissant en quoi le retrogaming peut avoir des intérêts aujourd'hui (écologie, préservation, découverte) et aussi en quoi le retrogaming est vivant. Ce projet est en partie dérivé d'un article que j'ai écrit, [Sa place n'est pas dans un musée](https://quarante-douze.net/sa-place-nest-pas-dans-un-musee).

Pour le dire de manière un peu "catchphrase" : Scanline est un site de *retrogaming vivant*.

## Ce que vous pourrez trouver ici

- Des informations sur comment émuler les jeux, utiliser des moteurs améliorés
- Des informations sur les néoconsoles, et le hacking de console retro
- Des liens sur ou vous pourrez trouver des romhacks, mods, etc.

## Updates and history

- (**202X-XX-XX**) Relancement du site

## Crédits

- Site généré grâce à [Eleventy](11ty.org)
- Icone provenant de la version 2.9 du pack d'icone de GNOME
- Fond d'écran venant originellement de GNOME
- Design inspiré du theme lilac de Windows 95