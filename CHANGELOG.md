# CHANGELOG

Tout les changements notables au projet sont consignés dans ce fichier, afin de simplement voir les évolutions.

Ce format est basé sur la norme [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
et ce projet adhère au [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## En cours

## [1.0.0] - 2020-10-01

Version initiale :

* Même base que rulebook
* Contient tout le travail présents dans kazhnuz.space
* Utilisation du branding kobold.city
